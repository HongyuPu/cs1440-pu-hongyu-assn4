import Deck
import Menu

class UserInterface():
    def __init__(self):
        pass


    def run(self):
        """Present the main menu to the user and repeatedly prompt for a valid command"""
        print("Welcome to the Bingo! Deck Generator\n")
        menu = Menu.Menu("Main")
        menu.addOption("C", "Create a new deck")
        
        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "C":
                self.__createDeck()
                pass
            elif command == "X":
                keepGoing = False


    def __createDeck(self):
        """Command to create a new Deck"""
        # TODO: Get the user to specify the card size, max number, and number of cards
        while True:
            self.__cardSize = input("Specify Card Size from 3 to 15: ")  # makes the user use correct values
            if self.__cardSize.isnumeric():
                self.__cardSize = int(self.__cardSize)
                if 3 <= self.__cardSize <= 15:
                    break
        while True:
            self.__numCards = input("Specify Number of Cards from 3 to 10,000: ")
            if self.__numCards.isnumeric():
                self.__numCards = int(self.__numCards)
                if 3 <= self.__numCards <= 10000:
                    break
        while True:
            self.__numMax = input("Specify Max Number from 2N^2 to 4N^2: ")
            if self.__numMax.isnumeric() or self.__numMax == "min" or self.__numMax == "max":
                if self.__numMax == "min":
                    self.__numMax = 2 * self.__cardSize ** 2
                elif self.__numMax == "max":
                    self.__numMax = 4 * self.__cardSize ** 2
                self.__numMax = int(self.__numMax)
                if 2 * self.__cardSize ** 2 <= self.__numMax <= 4 * self.__cardSize ** 2:
                    break

        # TODO: Create a new deck
        self.__m_currentDeck = Deck.Deck(self.__cardSize, self.__numCards, self.__numMax)

        # TODO: Display a deck menu and allow use to do things with the deck
        self.__deckMenu()
        pass


    def __deckMenu(self):
        """Present the deck menu to user until a valid selection is chosen"""
        menu = Menu.Menu("Deck")
        menu.addOption("P", "Print a card to the screen");
        menu.addOption("D", "Display the whole deck to the screen");
        menu.addOption("S", "Save the whole deck to a file");

        keepGoing = True
        while keepGoing:
            command = menu.show()
            if command == "P":
                self.__printCard()
            elif command == "D":
                print()
                self.__m_currentDeck.print()
            elif command == "S":
                self.__saveDeck()
            elif command == "X":
                keepGoing = False

    def __getNumberInput(self, text, cardToPrint, cardCount):
        cardToPrint = input(text)
        if cardToPrint.isnumeric():
            cardToPrint = int(cardToPrint)
            if 0 < cardToPrint <= cardCount:
                return cardToPrint
        else:
            return -1 # int(cardCount)

    def __printCard(self):
        """Command to print a single card"""
        x = True
        while x:
            cardToPrint = self.__getNumberInput("Id of card to print: ", 1, self.__m_currentDeck.getCardCount())
            if cardToPrint != None:
                if cardToPrint > 0:
                    x = False
                    print()
                    self.__m_currentDeck.print(idx=cardToPrint)

    def __getStringInput(self, words):
        return input(words)

    def __saveDeck(self):
        """Command to save a deck to a file"""
        fileName = self.__getStringInput("Enter output file name: ")
        if fileName != "":
            # TODO: open a file and pass to currentDeck.print()
            outputStream = open(fileName, 'w')
            self.__m_currentDeck.print(outputStream)
            outputStream.close()
            print("Done!")
