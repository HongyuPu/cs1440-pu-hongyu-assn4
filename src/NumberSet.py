import random

class NumberSet():
    def __init__(self, size):
        """NumberSet constructor"""
        self.__set = []  # generate number set 1 to size
        self.__size = size
        self.__count = 0
        for i in range(size):
            self.__set.append(i + 1)

    def getSize(self):
        """Return an integer: the size of the NumberSet"""
        return self.__size
        pass

    def get(self, index):
        """Return an integer: get the number from this NumberSet at an index"""
        if len(self.__set) != 0:
            return self.__set[index]
        pass


    def randomize(self):
        random.shuffle(self.__set)
        """void function: Shuffle this NumberSet"""
        pass


    def getNext(self):
        """Return an integer: when called repeatedly return successive values
        from the NumberSet until the end is reached, at which time 'None' is returned"""
        if len(self.__set) != 0 and self.__count <= len(self.__set):
            x = self.__set[self.__count]  # this is gay
            self.__count += 1
            return x
        else:
            return None
        pass
