import sys

import NumberSet

class Card():
    def __init__(self, idnum, size, numberSet):  # creates individual card for "for" loop in deck
        # """Card constructor"""
        self.__idnum = idnum  # makes idnumber with it
        self.__size = size
        self.__numberSet = numberSet
        self.card = NumberSet.NumberSet(numberSet)  # makes a card
        self.card.randomize()
        self.cards = []
        for i in range(size ** 2):  # appending to set and add free space
            self.cards.append(self.card.getNext())
        if size % 2 == 1:
            self.mid = int(size ** 2 / 2)  # added free space
            self.cards[self.mid] = "Free"
        pass

    def getId(self):
        """Return an integer: the ID number of the card"""
        return self.__idnum
        pass

    def getSize(self):
        """Return an integer: the size of one dimension of the card.
        A 3x3 card will return 3, a 5x5 card will return 5, etc.
        """
        return self.__size
        pass

    def print(self, file=sys.stdout):
        """void function:
        Prints a card to the screen or to an open file object"""
        if file != sys.stdout:
            for i in range(len(self.cards)):
                if i % self.__size == 0:
                    file.write('\n')
                    for j in range(self.__size):
                        file.write("+----")
                    file.write("+")
                    file.write('\n')
                    file.write("|")
                file.write((str(self.cards[i])).center(4))
                file.write("|")
            file.write("\n")
            for k in range(self.__size):
                file.write("+----")
            file.write("+")
            file.write('\n')
        else:
            for i in range(len(self.cards)):
                if i % self.__size == 0:
                    print()
                    for j in range(self.__size):
                        print("+----", end='')
                    print("+")
                    print("|", end='')
                print((str(self.cards[i])).center(4) + "|", end='')
            print()
            for k in range(self.__size):
                print("+----", end='')
            print("+")

        print()
        pass
